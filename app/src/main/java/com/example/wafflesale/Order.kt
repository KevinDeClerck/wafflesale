package com.example.wafflesale

class Order {


    var orderList : MutableList<OrderLine> = mutableListOf()

    fun addOrderLine(orderLine:OrderLine) {
        orderList.add(orderLine)
    }

    fun removeOrderLine(orderLine: OrderLine) {
        orderList.remove(orderLine)
    }

    fun printOrder() {
        orderList.forEach{o -> println(o)}
    }
}