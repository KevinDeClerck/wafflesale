package com.example.wafflesale.model

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.wafflesale.R
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_confirm_order.*

class ConfirmOrderActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_order)
        val totalPrice: TextView = findViewById(R.id.tv_totalPrice)
        val price: Double = intent.getDoubleExtra("totalPrice", 0.0)
        totalPrice.text = price.toString() + "€"
    }

    fun confirmOrder(iew: View) {
        val order = HashMap<String, Any>()
        order["name"] = tv_name.text.toString()
        order["firstname"] = tv_firstname.text.toString()
        order["phone"] = tv_phone.text.toString()
        order["cellphone"] = tv_cellphone.text.toString()
        order["price"] = tv_totalPrice.text.toString()

        // Add a new document with a generated ID
        db.collection("orders")
            .add(order)
            .addOnSuccessListener { documentReference ->
                println("succes")
                Toast.makeText(
                    this@ConfirmOrderActivity, "Bestelling geplaatst.",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .addOnFailureListener { e ->
                println("failure")
            }
        val intent = Intent(this, About::class.java)
        startActivity(intent)
    }

    fun goBackToOrder(view: View) {
        val intent = Intent(this, Bestellen::class.java)
        startActivity(intent)
    }
}
