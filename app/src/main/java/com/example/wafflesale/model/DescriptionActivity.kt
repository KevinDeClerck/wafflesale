package com.example.wafflesale.model

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.wafflesale.R

class DescriptionActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description)
        var descrptionImg: ImageView = findViewById(R.id.iv_description)
        var descriptions: TextView = findViewById(R.id.tv_description)
        var productName: TextView = findViewById(R.id.tv_productName)
        val productNames: String = intent.getStringExtra("product")
        val description: String = intent.getStringExtra("description")
        val descriptionsImg: Int? = intent.getIntExtra("photo", 0)
        if (descriptionsImg != null) {
            descrptionImg.setImageResource(descriptionsImg)
            descriptions.text = description
            productName.text = productNames
        }
    }

    fun goBackToList(view: View) {
        val intent = Intent(this, ProductList::class.java)
        startActivity(intent)
    }


}
