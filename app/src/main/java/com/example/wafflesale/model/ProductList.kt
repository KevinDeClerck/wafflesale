package com.example.wafflesale.model

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.wafflesale.R
import com.example.wafflesale.ShopListAdapter
import kotlinx.android.synthetic.main.activity_product_list.*

class ProductList : AppCompatActivity() {

    private var adapter: ShopListAdapter?=null
    private var productList: ArrayList<Product>?=null
    private var layoutManager: RecyclerView.LayoutManager?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        productList = ArrayList<Product>()
        layoutManager = LinearLayoutManager(this)
        adapter = ShopListAdapter(productList!!, this)
        recycler.layoutManager= layoutManager
        recycler.adapter = adapter
        recycler.setHasFixedSize(true)
//load data
        var productNameList: Array<String> = arrayOf(
            "Vanille wafels",
            "Chocolade wafels",
            "Franchipan",
            "Carré",
            "Mix",
            "10x Vanille wafels",
            "10x Chocolade wafels",
            "10x Franchipan",
            "10x Carré",
            "10x Mix"
        )
        var priceList: Array<Double> = arrayOf(7.00, 7.00, 8.00, 8.00, 8.00, 63.00, 63.00, 72.00, 72.00, 72.00)
        var images: Array<Int> = arrayOf(
            R.drawable.vanillew,
            R.drawable.chocow,
            R.drawable.franchipane,
            R.drawable.carre,
            R.drawable.mix,
            R.drawable.vanillewpromo,
            R.drawable.chocowpromo,
            R.drawable.franchipanepromo,
            R.drawable.carrepromo,
            R.drawable.mixpromo
        )
        var weightList: Array<String> =
            arrayOf("700g", "700g", "800g", "800g", "800g", "10x 700g", "10x 700g", "10x 800g", "10x 800g", "10x 800g")
        var discountList: Array<String> = arrayOf("", "", "", "", "", "-10%", "-10%", "-10%", "-10%", "-10%")
        var descriptionList: Array<String> = arrayOf(
            getString(R.string.description1),
            getString(R.string.description2),
            getString(R.string.description3),
            getString(R.string.description4),
            getString(R.string.description5),
            getString(R.string.description6),
            getString(R.string.description7),
            getString(R.string.description8),
            getString(R.string.description9),
            getString(R.string.description10)
        )

        for (i in 0..9) {
            var product = Product()
            product.product = productNameList[i]
            product.weight = weightList[i]
            product.price = priceList[i]
            product.photo = images[i]
            product.discount = discountList[i]
            product.description = descriptionList[i]
            productList?.add(product)

        }
        adapter!!.notifyDataSetChanged()
    }

    fun goToAbout(view: View) {
        val intent = Intent(this, About::class.java)
        startActivity(intent)
    }

}
