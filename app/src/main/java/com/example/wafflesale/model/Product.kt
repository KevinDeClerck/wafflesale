package com.example.wafflesale.model

class Product {
    var product:String?=null
    var price:Double?=null
    var photo:Int=0
    var weight:String?=null
    var discount: String? = null
    var description: String? = null
}