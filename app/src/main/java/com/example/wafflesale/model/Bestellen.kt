package com.example.wafflesale.model

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import com.example.wafflesale.R
import kotlinx.android.synthetic.main.activity_bestellen.*

class Bestellen() : AppCompatActivity() {

    private var progressView: TextView? = null
    private var seekbarView: SeekBar? = null
    private var progressView2: TextView? = null
    private var seekbarView2: SeekBar? = null
    private var progressView3: TextView? = null
    private var seekbarView3: SeekBar? = null
    private var progressView4: TextView? = null
    private var seekbarView4: SeekBar? = null
    private var progressView5: TextView? = null
    private var seekbarView5: SeekBar? = null
    private var progressView6: TextView? = null
    private var seekbarView6: SeekBar? = null
    private var progressView7: TextView? = null
    private var seekbarView7: SeekBar? = null
    private var progressView8: TextView? = null
    private var seekbarView8: SeekBar? = null
    private var progressView9: TextView? = null
    private var seekbarView9: SeekBar? = null
    private var progressView10: TextView? = null
    private var seekbarView10: SeekBar? = null

    var progress1: Int = 0
    var progress2: Int = 0
    var progress3: Int = 0
    var progress4: Int = 0
    var progress5: Int = 0
    var progress6: Int = 0
    var progress7: Int = 0
    var progress8: Int = 0
    var progress9: Int = 0
    var progress10: Int = 0

    var calculatedPrice: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bestellen)

        progressView = findViewById(R.id.tv_seekbar)
        seekbarView = findViewById(R.id.seekbar)
        progressView2 = findViewById(R.id.tv_seekbar2)
        seekbarView2 = findViewById(R.id.seekbar2)
        progressView3 = findViewById(R.id.tv_seekbar3)
        seekbarView3 = findViewById(R.id.seekbar3)
        progressView4 = findViewById(R.id.tv_seekbar4)
        seekbarView4 = findViewById(R.id.seekbar4)
        progressView5 = findViewById(R.id.tv_seekbar5)
        seekbarView5 = findViewById(R.id.seekbar5)
        progressView6 = findViewById(R.id.tv_seekbar6)
        seekbarView6 = findViewById(R.id.seekbar6)
        progressView7 = findViewById(R.id.tv_seekbar7)
        seekbarView7 = findViewById(R.id.seekbar7)
        progressView8 = findViewById(R.id.tv_seekbar8)
        seekbarView8 = findViewById(R.id.seekbar8)
        progressView9 = findViewById(R.id.tv_seekbar9)
        seekbarView9 = findViewById(R.id.seekbar9)
        progressView10 = findViewById(R.id.tv_seekbar10)
        seekbarView10 = findViewById(R.id.seekbar10)



        seekbarView?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView!!.text = progress.toString()
                progress1 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView2?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView2!!.text = progress.toString()
                progress2 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView3?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView3!!.text = progress.toString()
                progress3 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView4?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView4!!.text = progress.toString()
                progress4 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView5?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView5!!.text = progress.toString()
                progress5 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView6?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView6!!.text = progress.toString()
                progress6 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView7?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView7!!.text = progress.toString()
                progress7 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView8?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView8!!.text = progress.toString()
                progress8 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView9?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView9!!.text = progress.toString()
                progress9 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        seekbarView10?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressView10!!.text = progress.toString()
                progress10 = progress
                calculatedPrice = calculateTotal()
                tv_total.text = calculatedPrice.toString() + "0€"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

    }

    fun calculateTotal(): Double {
        var totaal: Double = 0.0
        totaal = ((progress1 * 7) + (progress2 * 7) + (progress3 * 8) + (progress4 * 8)
                + (progress5 * 8) + (progress6 * 63) + (progress7 * 63) + (progress8 * 72)
                + (progress9 * 72) + (progress10 * 72)).toDouble()
        if (totaal < 80.00) {
            return totaal
        } else {
            return totaal * 0.9
        }
    }

    fun goToConfirmm(view: View) {
        val price: Double = calculatedPrice
        val intent = Intent(this, ConfirmOrderActivity::class.java)
        intent.putExtra("totalPrice", price)
        startActivity(intent)
    }

    fun goBack(view: View) {
        val intent = Intent(this, About::class.java)
        startActivity(intent)
    }
}

