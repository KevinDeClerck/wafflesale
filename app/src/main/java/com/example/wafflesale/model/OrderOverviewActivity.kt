package com.example.wafflesale.model

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.wafflesale.R
import com.google.firebase.firestore.FirebaseFirestore

class OrderOverviewActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_overview)

        val docRef = db.collection("orders")
        docRef.get()
            .addOnSuccessListener { result ->
                if (result!= null) {
                    println("succes")
                } else {
                    println("Rip")
                }
            }
            .addOnFailureListener { exception ->
                println("Rip")
            }

    }
}
