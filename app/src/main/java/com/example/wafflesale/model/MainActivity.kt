package com.example.wafflesale.model

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.example.wafflesale.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var email: String? = null
    private var password: String? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialise()
    }

    private fun initialise() {
        mAuth = FirebaseAuth.getInstance()
        btn_login!!.setOnClickListener {
            email = et_loginemail.text.toString()
            password = et_loginpass.text.toString()
            loginUser()
        }
    }

    private fun loginUser() {

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            mAuth!!.signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(
                            this@MainActivity, "Succesvol aangemeld.",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this@MainActivity, About::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            this@MainActivity, "Fout bij e-mail of passwoord.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }

    fun goToRegister(view: View) {
        val intent = Intent(this, CreateAccountActivity::class.java)
        startActivity(intent)
    }
}
