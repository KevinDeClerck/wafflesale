package com.example.wafflesale

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.wafflesale.model.DescriptionActivity
import com.example.wafflesale.model.Product

class ShopListAdapter (private val list:ArrayList<Product>, private val context:Context): RecyclerView.Adapter<ShopListAdapter.ViewHolder>(){


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(product: Product) {
            var productName: TextView = itemView.findViewById(R.id.tv_product) as TextView
            var price: TextView = itemView.findViewById(R.id.tv_price) as TextView
            var productWeight: TextView = itemView.findViewById(R.id.tv_weight) as TextView
            var dishPics: ImageView = itemView.findViewById(R.id.imageView) as ImageView
            var discount: TextView = itemView.findViewById(R.id.tv_discount) as TextView
            productWeight.text = product.weight
            dishPics.setImageResource(product.photo)
            productName.text = product.product
            price.text = "Prijs: " + product.price.toString() + "0€"
            discount.text = product.discount


            itemView.setOnClickListener {
                Toast.makeText(context, product.product, Toast.LENGTH_SHORT).show()
                val intent = Intent(context, DescriptionActivity::class.java)
                intent.putExtra("photo", product.photo)
                intent.putExtra("description", product.description)
                intent.putExtra("product", product.product)
                context.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ShopListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_view, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    override fun onBindViewHolder(holder: ShopListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }

}