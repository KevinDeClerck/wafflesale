package com.example.wafflesale

enum class ProductKind(val sName:String,val price:Float, val weight:Float) {
    CHOCO_WAFFLE("Chocowaffle",6f,700f),
    VANILLA_WAFFLE("Vanillawaffle",6f,700f),
    FRANCHIPAN("Frnachipan",7f,700f),
    CARRE("Carre",7f,700f),
    MIX("Mix",7f,800f);
}