package com.example.wafflesale

class Producten(productKind:ProductKind) {
    var productKind = productKind
    var price = this.productKind.price

    fun getProductDescription():String{
        if (productKind.equals("MIX")){
            return "Dit is een combinatie van blablalblalbla"
        }else
            return this.productKind.sName
    }

    override fun toString(): String {
        return "Producten(productKind=${productKind.sName}, price=$price \nDestription : ${getProductDescription()})"
    }


}