package com.example.wafflesale

class Leden(firstName:String, lastName:String) : Persoon(firstName,lastName) {
    var orders : MutableList<Order> = mutableListOf()

    fun addOrder(order: Order){
        orders.add(order)
    }

    fun removeOrder(order: Order){
        orders.remove(order)
    }



}