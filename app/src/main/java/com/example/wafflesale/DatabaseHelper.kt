package com.example.wafflesale

import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

object DatabaseHelper : OrmLiteSqliteOpenHelper (App.instance, "waffleApp.db", null,1){
    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTableIfNotExists(connectionSource,OrderLine::class.java)
    }

    override fun onUpgrade(
        database: SQLiteDatabase?,
        connectionSource: ConnectionSource?,
        oldVersion: Int,
        newVersion: Int
    ) {
        TableUtils.dropTable<OrderLine,Any>(connectionSource,OrderLine::class.java,true)
        DatabaseHelper.onCreate(database,connectionSource)
    }
}