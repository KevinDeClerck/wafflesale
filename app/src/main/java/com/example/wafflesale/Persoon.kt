package com.example.wafflesale

abstract class Persoon(firstName:String, lastName:String) {

    open val firstName = firstName
    open val lastName = lastName
}