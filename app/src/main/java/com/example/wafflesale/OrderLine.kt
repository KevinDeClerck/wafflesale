package com.example.wafflesale

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable


@DatabaseTable(tableName = "orderline")
data class OrderLine (

    @DatabaseField(id = true)
    var orderLineId:Int?=null,

    @DatabaseField
    var product: Producten?=null,

    @DatabaseField
    var amount: Int = 0


)

        class OrderLineDAO{


            companion object {
                lateinit var dao: Dao<OrderLine, Int>
            }
            init {
                dao = DatabaseHelper.getDao(OrderLine::class.java)
            }

        }